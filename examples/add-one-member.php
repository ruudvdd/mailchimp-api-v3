<?php
require '../vendor/autoload.php';

// create factory with username and apikey
$factory = new \Mailchimp\Factory('foo', 'xxxssssss-xx');

/** @var Mailchimp\Endpoint\Lists $endpoint */
$endpoint = $factory->build(\Mailchimp\Factory::LISTS);

// create member you want to add
$member = (new \Mailchimp\Entity\Member())
    ->setEmailAddress('foobar@example.org')
    ->setMergeFields(new \Mailchimp\Value\MergeTags([
        'FNAME' => 'foo',
        'LNAME' => 'bar'
    ]));

// create and send request
$response = $endpoint->addMember('xxx', $member, true)->send();

if ($response instanceof \Mailchimp\Http\Response\ErrorResponse) {
    // do something when Mailchimp returned an error
} else {
    // do something when the member is successfully created
}