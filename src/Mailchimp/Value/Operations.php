<?php
namespace Mailchimp\Value;

class Operations implements \JsonSerializable
{
    /**
     * @var Operation[]
     */
    protected $list;

    public function __construct($operations = [])
    {
        $this->setOperations($operations);
    }

    /**
     * replaces the current stored merge tags
     *
     * @param Operation[] $operations array of operations
     * @return $this
     */
    public function setOperations(array $operations)
    {
        $this->resetOperations();
        $this->addOperations($operations);
        return $this;
    }

    public function resetOperations()
    {
        $this->list = [];
    }

    /**
     * @param Operation $operation
     * @return $this
     */
    public function addOperation(Operation $operation)
    {
        $this->list[] = $operation;
        return $this;
    }

    /**
     * adds to the current stored merge tags
     *
     * @param Operation[] $operations assoc array of merge tags
     * @return $this
     */
    public function addOperations(array $operations)
    {
        foreach ($operations as $name) {
            $this->addOperation($name);
        }
        return $this;
    }

    /**
     * @return Operation[]
     */
    public function getOperations()
    {
        return $this->list;
    }

    /**
     * @inheritdoc
     */
    function jsonSerialize()
    {
        return $this->getOperations();
    }
}
