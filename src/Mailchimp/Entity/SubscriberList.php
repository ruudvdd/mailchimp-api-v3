<?php
namespace Mailchimp\Entity;

use Mailchimp\Value\Contact;
use Mailchimp\Value\Campaign;

class SubscriberList implements \JsonSerializable
{
    const VISIBILITY_PUB = true;
    const VISIBILITY_PRV = false;

    /**
     * @var int|null
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var Contact
     */
    protected $contact;

    /**
     * @var string
     */
    protected $permissionReminder;

    /**
     * @var bool
     */
    protected $useArchiveBar;

    /**
     * @var Campaign\Defaults
     */
    protected $campaignDefaults;

    /**
     * @var string
     */
    protected $notifyOnSubscribe;

    /**
     * @var string
     */
    protected $notifyOnUnsubscribe;

    /**
     * @var bool
     */
    protected $emailTypeOption;

    /**
     * @var bool
     */
    protected $visibility;

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     * @return $this
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
        return $this;
    }

    /**
     * @return string
     */
    public function getPermissionReminder()
    {
        return $this->permissionReminder;
    }

    /**
     * @param string $permissionReminder
     * @return $this
     */
    public function setPermissionReminder($permissionReminder)
    {
        $this->permissionReminder = $permissionReminder;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isUseArchiveBar()
    {
        return $this->useArchiveBar;
    }

    /**
     * @param boolean $useArchiveBar
     * @return $this
     */
    public function setUseArchiveBar($useArchiveBar)
    {
        $this->useArchiveBar = $useArchiveBar;
        return $this;
    }

    /**
     * @return Campaign\Defaults
     */
    public function getCampaignDefaults()
    {
        return $this->campaignDefaults;
    }

    /**
     * @param Campaign\Defaults $campaignDefaults
     * @return $this
     */
    public function setCampaignDefaults($campaignDefaults)
    {
        $this->campaignDefaults = $campaignDefaults;
        return $this;
    }

    /**
     * @return string
     */
    public function getNotifyOnSubscribe()
    {
        return $this->notifyOnSubscribe;
    }

    /**
     * @param string $notifyOnSubscribe
     * @return $this
     */
    public function setNotifyOnSubscribe($notifyOnSubscribe)
    {
        $this->notifyOnSubscribe = $notifyOnSubscribe;
        return $this;
    }

    /**
     * @return string
     */
    public function getNotifyOnUnsubscribe()
    {
        return $this->notifyOnUnsubscribe;
    }

    /**
     * @param string $notifyOnUnsubscribe
     * @return $this
     */
    public function setNotifyOnUnsubscribe($notifyOnUnsubscribe)
    {
        $this->notifyOnUnsubscribe = $notifyOnUnsubscribe;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isEmailTypeOption()
    {
        return $this->emailTypeOption;
    }

    /**
     * @param boolean $emailTypeOption
     * @return $this
     */
    public function setEmailTypeOption($emailTypeOption)
    {
        $this->emailTypeOption = $emailTypeOption;
        return $this;
    }

    /**
     * @return bool true if the list is public, false if it's private
     */
    public function isVisibility()
    {
        return $this->visibility;
    }

    /**
     * @param bool $visibility
     * @return $this
     */
    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;
        return $this;
    }

    /**
     * @inheritdoc
     */
    function jsonSerialize()
    {
        $jsonSerialize = [];
        if ($this->getId() !== null) {
            $jsonSerialize['id'] = $this->getId();
        }

        $jsonSerialize += [
            'name'                  => $this->getName(),
            'contact'               => $this->getContact(),
            'permission_reminder'   => $this->getPermissionReminder(),
            'use_archive_bar'       => $this->isUseArchiveBar(),
            'campaign_defaults'     => $this->getCampaignDefaults(),
            'notify_on_subscribe'   => $this->getNotifyOnSubscribe(),
            'notify_on_unsubscribe' => $this->getNotifyOnUnsubscribe(),
            'email_type_option'     => $this->isEmailTypeOption(),
            'visibility'            => $this->isVisibility() ? 'pub' : 'prv'
        ];

        return $jsonSerialize;
    }
}
