<?php
namespace Mailchimp\Entity;

use Mailchimp\Value\Location;
use Mailchimp\Value\MergeTags;

class Member implements \JsonSerializable
{
    const EMAIL_TYPE_HTML = 'html';
    const EMAIL_TYPE_TEXT = 'test';

    const STATUS_SUBSCRIBED = 'subscribed';
    const STATUS_UNSUBSCRIBED = 'unsubscribed';
    const STATUS_CLEANED = 'cleaned';
    const STATUS_PENDING = 'pending';

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $emailAddress;

    /**
     * @var string html or text
     */
    protected $emailType;

    /**
     * @var string subscribed, unsubscribed, cleaned or pending
     */
    protected $status;

    /**
     * @var MergeTags
     */
    protected $mergeFields;

    /**
     * @var array
     */
    protected $interests;

    /**
     * @var string
     */
    protected $language;

    /**
     * @var bool
     */
    protected $vip;

    /**
     * @var Location
     */
    protected $location;

    /**
     * @var string
     */
    protected $ipOpt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * @param string $emailAddress
     * @return $this
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmailType()
    {
        return $this->emailType;
    }

    /**
     * @param string $emailType
     * @return $this
     */
    public function setEmailType($emailType)
    {
        $this->emailType = $emailType;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return MergeTags
     */
    public function getMergeFields()
    {
        return $this->mergeFields;
    }

    /**
     * @param MergeTags $mergeFields
     * @return $this
     */
    public function setMergeFields($mergeFields)
    {
        $this->mergeFields = $mergeFields;
        return $this;
    }

    /**
     * @return array
     */
    public function getInterests()
    {
        return $this->interests;
    }

    /**
     * @param array $interests
     * @return $this
     */
    public function setInterests($interests)
    {
        $this->interests = $interests;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     * @return $this
     */
    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isVip()
    {
        return $this->vip;
    }

    /**
     * @param boolean $vip
     * @return $this
     */
    public function setVip($vip)
    {
        $this->vip = $vip;
        return $this;
    }

    /**
     * @return Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param Location $location
     * @return $this
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return string
     */
    public function getIpOpt()
    {
        return $this->ipOpt;
    }

    /**
     * @param string $ipOpt
     * @return $this
     */
    public function setIpOpt($ipOpt)
    {
        $this->ipOpt = $ipOpt;
        return $this;
    }

    /**
     * @inheritdoc
     */
    function jsonSerialize()
    {
        $memberSerialized = [];
        if ($this->getId() !== null) {
            $memberSerialized['id'] = $this->getId();
        }

        $memberSerialized += [
            'email_address' => $this->getEmailAddress(),
            'email_type'    => $this->getEmailType(),
            'status'        => $this->getStatus(),
            'merge_fields'  => $this->getMergeFields(),
            'interests'     => $this->getInterests(),
            'language'      => $this->getLanguage(),
            'vip'           => $this->isVip(),
            'location'      => $this->getLocation(),
            'ip_opt'        => $this->getIpOpt()
        ];

        return array_filter($memberSerialized);
    }
}
