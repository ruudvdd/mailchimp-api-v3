<?php
namespace Mailchimp;

use GuzzleHttp\Client as GuzzleClient;
use Mailchimp\Converter\RequestToOperation;
use Mailchimp\Converter\RequestToOperationConverterAware;
use Mailchimp\Endpoint;
use Mailchimp\Http\Client;

class Factory
{
    const BLANK = 'blank';
    const LISTS = 'lists';
    const BATCHES = 'batches';

    protected static $library = [
        self::BLANK => 'Mailchimp\Endpoint\Blank',
        self::LISTS => 'Mailchimp\Endpoint\Lists',
        self::BATCHES => 'Mailchimp\Endpoint\Batches'
    ];

    /**
     * @param string $username
     * @param string $apiKey
     * @param array $guzzleClientOptions
     */
    public function __construct($username, $apiKey, $guzzleClientOptions = [])
    {
        $options['base_uri'] = $this->buildBaseUri($apiKey);
        $client = new GuzzleClient($options);

        $this->client = new Client($client, $username, $apiKey);
    }

    /**
     * 
     * @param string $endpointType
     * @return Endpoint\AbstractEndpoint
     */
    public function build($endpointType)
    {
        if (!isset(self::$library[$endpointType])) {
            // TODO throw new
            return null;
        }

        $endpoint = new self::$library[$endpointType]($this->client);

        if ($endpoint instanceof RequestToOperationConverterAware) {
            $endpoint->setRequestToOperation(new RequestToOperation());
        }

        return $endpoint;
    }

    /**
     * @param string $apiKey
     * @return string
     */
    private function buildBaseUri($apiKey)
    {
        list($firstPart, $server) = explode('-', $apiKey);
        return sprintf(Client::BASE_URL, $server);
    }
}
