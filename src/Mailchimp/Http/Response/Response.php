<?php
namespace Mailchimp\Http\Response;

class Response extends AbstractResponse
{

    /**
     * @param string $name
     * @return string|null
     */
    public function get($name)
    {
        $decodedResponseBody = $this->getDecodedResponseBody();
        if (!isset($decodedResponseBody[$name])) {
            return null;
        }
        return $decodedResponseBody[$name];
    }
}
