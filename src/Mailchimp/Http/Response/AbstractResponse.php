<?php
namespace Mailchimp\Http\Response;

use Psr\Http\Message\ResponseInterface;

abstract class AbstractResponse
{
    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * @var string
     */
    protected $decodedResponseBody;

    /**
     * @param ResponseInterface $response
     */
    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
    }

    /**
     * @return ResponseInterface
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param ResponseInterface $response
     * @return $this
     */
    public function setResponse($response)
    {
        $this->response = $response;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDecodedResponseBody()
    {
        if ($this->decodedResponseBody === null) {
            $this->decodedResponseBody = json_decode($this->getResponse()->getBody(), true);
        }
        return $this->decodedResponseBody;
    }
}
