<?php
namespace Mailchimp\Http;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use Mailchimp\Http\Response\ErrorResponse;
use Psr\Http\Message\ResponseInterface;

class Client
{
    const BASE_URL = 'https://%s.api.mailchimp.com';
    const VERSION = '3.0';

    /**
     * @var GuzzleClient
     */
    protected $client;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @param GuzzleClient $client
     * @param $username
     * @param $apiKey
     */
    public function __construct(GuzzleClient $client, $username, $apiKey)
    {
        $this->client = $client;
        $this->username = $username;
        $this->apiKey = $apiKey;
    }

    /**
     * @return GuzzleClient
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param GuzzleClient $client
     * @return $this
     */
    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     * @return $this
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        return $this;
    }



    /**
     * @param Request $request
     * @return ResponseInterface
     */
    public function send(Request $request)
    {
        $response = null;
        try {
            $response = $this->client->send($request->getRequest());
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }
        return $response;
    }


    /**
     * @param $endpoint
     * @param $method
     * @param array $queryData
     * @param array|\JsonSerializable $postData
     * @return Request
     */
    public function createRequest($endpoint, $method, $queryData = [], $postData = null)
    {
        $query = http_build_query($queryData);

        $headers = ['Authorization' => $this->getAuthorizationHeader()];
        $endpointPath = $this->buildEndpointPath($endpoint) . '?' . $query;
        $postData = $postData === null ? null : json_encode($postData);
        $request = new GuzzleRequest($method, $endpointPath, $headers, $postData);
        return new Request($this, $request);
    }

    /**
     * @return string
     */
    private function createBasicAuthToken()
    {
        return base64_encode($this->username . ':' . $this->apiKey);
    }

    /**
     * @param $endpoint
     * @return string
     */
    private function buildEndpointPath($endpoint)
    {
        return '/' . self::VERSION . '/' . ltrim($endpoint, '/');
    }

    /**
     * @return string
     */
    private function getAuthorizationHeader()
    {
        return 'Basic ' . $this->createBasicAuthToken();
    }
}
