<?php
namespace Mailchimp\Converter;

use Mailchimp\Http\Request;
use Mailchimp\Value\Operation;

class RequestToOperation
{
    /**
     * @param Request[] $requests
     * @return Operation[]
     */
    public function many(array $requests)
    {
        $operations = [];
        foreach ($requests as $request) {
            $operations[] = $this->one($request);
        }
        return $operations;
    }

    /**
     * @param Request $request
     * @return Operation
     */
    public function one(Request $request)
    {
        $guzzleRequest = $request->getRequest();
        $operation = new Operation();
        $path = ltrim($guzzleRequest->getUri()->getPath(), '/');
        $segments = explode('/', $path);
        // remove the version of the api
        unset($segments[0]);

        $operation->setPath('/' . implode('/', $segments))
            ->setMethod($guzzleRequest->getMethod())
            ->setBody($guzzleRequest->getBody()->getContents());
        return $operation;
    }
}
