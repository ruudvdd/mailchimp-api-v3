<?php
namespace Mailchimp\Endpoint;

class Blank extends AbstractEndpoint
{
    /**
     * @param string $url
     * @param string $method
     * @param array $queryData
     * @param \JsonSerializable|array $postData
     * @return \Mailchimp\Http\Request
     */
    public function getRequest($url, $method, array $queryData = [], $postData = [])
    {
        return $this->client->createRequest($url, $method, $queryData, $postData);
    }

    /**
     * @param string $url
     * @param array $queryData
     * @return \Mailchimp\Http\Request
     */
    public function get($url, array $queryData = [])
    {
        return $this->getRequest($url, 'GET', $queryData);
    }

    /**
     * @param string $url
     * @param array $queryData
     * @param \JsonSerializable|array $postData
     * @return \Mailchimp\Http\Request
     */
    public function post($url, array $queryData = [], $postData = [])
    {
        return $this->getRequest($url, 'POST', $queryData, $postData);
    }

    /**
     * @param string $url
     * @param array $queryData
     * @param \JsonSerializable|array $postData
     * @return \Mailchimp\Http\Request
     */
    public function put($url, array $queryData = [], $postData = [])
    {
        return $this->getRequest($url, 'PUT', $queryData, $postData);
    }

    /**
     * @param string $url
     * @param array $queryData
     * @param \JsonSerializable|array $postData
     * @return \Mailchimp\Http\Request
     */
    public function patch($url, array $queryData = [], $postData = [])
    {
        return $this->getRequest($url, 'PATCH', $queryData, $postData);
    }

    /**
     * @param string $url
     * @param array $queryData
     * @return \Mailchimp\Http\Request
     */
    public function delete($url, array $queryData = [])
    {
        return $this->getRequest($url, 'DELETE', $queryData);
    }
}
