<?php
namespace Mailchimp\Endpoint;

use Mailchimp\Converter\RequestToOperation;
use Mailchimp\Converter\RequestToOperationConverterAware;
use Mailchimp\Http\Request;
use Mailchimp\Value\Operations;

class Batches extends AbstractEndpoint implements RequestToOperationConverterAware
{
    /**
     * @var RequestToOperation
     */
    protected $requestToOperation;

    /**
     * @param Operations $batch
     * @return Request
     */
    public function addBatch(Operations $batch)
    {
        $request = $this->client->createRequest('/batches', 'POST', [], ['operations' => $batch]);
        return $request;
    }

    /**
     * @param Request[] $requests
     * @return Request
     */
    public function addBatchFromRequests(array $requests)
    {
        return $this->addBatch(new Operations($this->requestToOperation->many($requests)));
    }

    /**
     * @param $batchId
     * @return Request
     */
    public function getBatch($batchId)
    {
        return $this->client->createRequest('/batches/' . $batchId, 'GET');
    }

    /**
     * @return Request
     */
    public function getAll()
    {
        return $this->client->createRequest('/batches', 'GET');
    }

    /**
     * @param RequestToOperation $requestToOperation
     * @return mixed
     */
    public function setRequestToOperation(RequestToOperation $requestToOperation)
    {
        $this->requestToOperation = $requestToOperation;
    }
}
