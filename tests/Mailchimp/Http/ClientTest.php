<?php
namespace Mailchimp\Http;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

class ClientTest extends \PHPUnit_Framework_TestCase
{
    const TEST_API_KEY = 'api_key';
    const TEST_USERNAME = 'username';

    /**
     * @var Client
     */
    protected $client;

    public function setup()
    {
    }

    protected function getClient($error = false)
    {
        $guzzleClient = $this->getMockBuilder('GuzzleHttp\Client')
            ->setConstructorArgs([])
            ->getMock();

        if ($error) {
            $guzzleClient
                ->method('send')
                ->willThrowException(new RequestException('', $this->getRequest()->getRequest(), new Response()));
        }

        return new Client($guzzleClient, self::TEST_USERNAME, self::TEST_API_KEY);
    }

    protected function getRequest()
    {
        $path = '/';
        $guzzleUri = $this->getMockBuilder('GuzzleHttp\Psr7\Uri')
            ->setConstructorArgs([$path])
            ->getMock();
        $guzzleUri->method('getPath')->willReturn('/v3.0/' . ltrim($path, '/'));

        $guzzleRequest = $this->getMockBuilder('GuzzleHttp\Psr7\Request')
            ->setConstructorArgs(['GET', $guzzleUri, [], '', '1.1'])
            ->getMock();

        $mailchimpRequest = $this->getMockBuilder('Mailchimp\Http\Request')
            ->setConstructorArgs([$this->getClient(), $guzzleRequest])
            ->getMock();


        $mailchimpRequest->method('getRequest')->willReturn($guzzleRequest);
        return $mailchimpRequest;
    }

    public function testClientSendReturnsResponseOnError()
    {
        $client = $this->getClient(true);
        $response = $client->send($this->getRequest());
        $this->assertInstanceOf(ResponseInterface::class, $response);
    }

    public function testCreateRequestSetsCorrectAuthorizationHeaders()
    {
        $client = $this->getClient();
        $request = $client->createRequest('/lists', 'GET');
        $guzzleRequest = $request->getRequest();
        $authorization = $guzzleRequest->getHeader('Authorization');
        $this->assertEquals('Basic dXNlcm5hbWU6YXBpX2tleQ==', current($authorization));
    }

    public function testCreateRequestAddsVersionToPath()
    {
        $client = $this->getClient();
        $path = '/lists';
        $request = $client->createRequest($path, 'GET');
        $guzzleRequest = $request->getRequest();

        $this->assertEquals('/' . Client::VERSION . $path, $guzzleRequest->getUri()->getPath());
    }
}
